ProstUuid new-type wrapper around uuid::Uuid with prost::Message implemented for it.

## How to use it

Create a protobuf file for uuid (will be provided out-of-the-box in future release):

```proto
syntax = "proto3";
package uuid;

message Uuid {
   string uuid_str = 1;
}
```

and use it in your own protobuf:

```proto
message User {
  uuid.Uuid id = 1;
  string email = 2;
}
```

in Rust code you will be able to use `ProstUuid` interchangably with `uuid::Uuid`
via different methods and dereferencing:

```rust
    fn test_derive_more() {
        let prost_uuid = ProstUuid::from(Uuid::nil());
        let uuid = Uuid::from(prost_uuid);
        assert_eq!(format!("{}", uuid), format!("{}", prost_uuid));
        let new_prost_uuid = ProstUuid::new(uuid);
        let mut mut_prost_uuid = new_prost_uuid;
        let another_prost_uuid = new_prost_uuid;
        function_expect_uuid(
            new_prost_uuid.as_ref(),
            mut_prost_uuid.as_mut(),
            *another_prost_uuid,
        );
    }

    fn function_expect_uuid(_uuid_ref: &Uuid, _uuid_mut_ref: &mut Uuid, _uuid: Uuid) {}
```
